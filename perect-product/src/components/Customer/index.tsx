import React from 'react';
import './style.scss';

export interface CustomerProps {
  source: string;
  name: string;
  text: string;
  position: string;
}
export default class Customer extends React.Component<CustomerProps, any> {
  render() {
      return (<div className="customer">
      <img src={this.props.source} alt={this.props.name}/>
      <p>{this.props.text}</p>
      <div className="headline-container">
        <h5>{this.props.name}</h5>
        <h6>{this.props.position}</h6>
      </div>
    </div>);
  }
}