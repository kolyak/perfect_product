import React from 'react';
import Content from './content';

export interface ProductSourceProps {
  source: string;
  name: string;
}
export default class Product extends React.Component<ProductSourceProps, any> {
  render() {
      return <Content {...this.props}/>
  }
}