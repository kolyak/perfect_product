import React from 'react';
import './style.scss';
import {ProductSourceProps} from './index';
export default class Content extends React.Component<ProductSourceProps> {
    render() {
        return (<div className="product">
          <img src={this.props.source} alt={this.props.name}/>
          <p>{this.props.name}</p>
        </div>);
    }
}
