import React from 'react';
import Logo from '../../images/logo.png';
import { Link }  from 'react-router-dom';
import './style.scss';
interface FunctionProps {
  openNav: any;
  logOut: any;
  linksContainer: string;
  email: string|null;
  isAuthorized: boolean;
}
export default class Content extends React.Component<FunctionProps, any> {


  render() {
      return (<header>
        <button className="sidenav-button" onClick={this.props.openNav}>&#9776;</button>
        <Link to="/" className="imgContainer">  
          <img src={Logo} className="logo" alt="logo" />
        </Link>
        <div className={`header-links ${this.props.linksContainer}`} >
          <button className="sidenav-button close" onClick={this.props.openNav}>&times;</button>
          <Link to="/new-product">ALL PRODUCTS</Link>
          <Link to="/">ABOUT US</Link>
          {(this.props.isAuthorized) ?
          <Link to="/profile" className="log-in">{this.props.email}</Link>
          :
          <Link to="/log-in" className="log-in">{this.props.email}</Link>}
          {(this.props.isAuthorized) ?
          <Link to="/" onClick={this.props.logOut} >LOG OUT</Link>
          :
          <Link to="/sign-up">SIGN UP</Link>}
        </div>
      </header>);
  }
}
