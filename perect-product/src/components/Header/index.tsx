import React from 'react';
import Content from './content';
import api from '../../services/api';
import { ToastContainer } from 'react-toastify';
interface HeaderState {
  linksContainer: string;
  email: string | null;
  isAuthorized: boolean;
}
export default class Header extends React.Component<any, HeaderState> {
  constructor(props: any) {
    super(props);
    this.state = {
      linksContainer: "",
      email: (!!api.getEmail()) ? api.getEmail() : "LOG IN",
      isAuthorized: (!!api.checkToken())
    }
  }

  logOut = () => {
    api.logOut();
    this.setState({
      email: "LOG IN",
      isAuthorized: false
    });
  }
  
  openNav = (e: any ) => {
    if(this.state.linksContainer === "") {
      this.setState({ linksContainer: " nav-links"})
    }
    else {
      this.setState({ linksContainer: ""})
    }
  }
  
  render() {
      return <div>  
          <ToastContainer />
          <Content logOut={this.logOut} isAuthorized={this.state.isAuthorized} email={this.state.email} openNav={this.openNav} linksContainer={this.state.linksContainer}/>
      </div>
  }
}