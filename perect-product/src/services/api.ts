import axios from "axios";

axios.defaults.baseURL = 'https://ski-rent-api.herokuapp.com';
axios.defaults.headers.post['Content-Type'] = 'application/json';

class Api {
  async get(url: string) {
    this.checkToken();
    return await axios.get(url);
  }

  async post(url: string, data: any) {
    this.checkToken();
    return await axios.post(url, data);
  }

  async patch(url: string, data: any) {
    this.checkToken();
    return await axios.patch(url, data);
  }

  save(email: string, tocken: any) {
    localStorage.setItem('token', tocken);
    localStorage.setItem('email', email);
  }

  getEmail() {
    return localStorage.getItem('email');
  }

  deleteToken() {
    delete localStorage['token'];
  }

  logOut() {
    localStorage.clear();
  }

  checkToken(): boolean {
    if (!!localStorage.getItem('token')) {
      axios.defaults.headers['Authorization'] = "Bearer " + localStorage.getItem('token'); 
      return true;
    }
    return false;
  }
}

export default new Api();