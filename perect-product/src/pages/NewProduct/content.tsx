import React from 'react';
import './style.scss';

interface State {
  categoryId: number
}

export default class Content extends React.Component<any, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      categoryId: 0
    }
  }
  
  render() {
    return (<div className="new-product-form">
      <form action="">
        <h2>New product</h2>
        { (!!this.props.categories)?
        (<select className="category" id="category" onChange={(val) => {this.setState({categoryId: +val.target.value})}}>
          {this.props.categories.map((category: any, index: number) => (
            <option key={index} value={index}> {category.name} </option>
          ))}
        </select>
        ) : ""}
        {
        (!!this.props.categories && !!this.props.categories.filters)?
          (this.props.categories[this.state.categoryId].filters.map((filter: any) => (
            filter.options.map((option: any, index: number) => (
              <div>
                <input  key={index} type="checkbox" value={option.id}/> {option.option_value} <br/>
              </div>
            ))
          ))

          ) : ""}  
        
      </form>
    </div>);
  }
}