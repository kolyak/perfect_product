import React from 'react';
import Content from './content';
import Header from '../../components/Header';
import api from '../../services/api';
import { toast } from 'react-toastify';

export default class NewProduct extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
    this.getCategories();
  }

  getCategories = async () => {
    try {
      let responce = await api.get("/api/categories");
      this.setState(responce.data);
    } catch (error) {
      toast.error(""+error);
      this.props.history.push("/log-in");
    }
  }

  render() {
    return <div>  
      <Header/>
      <Content categories={this.state.categories}/>
      </div>
  }
}