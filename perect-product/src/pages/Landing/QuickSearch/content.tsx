import { Link }  from 'react-router-dom';
import React from 'react';
import AllCategories from '../../../images/icon_category.png';
import HowItsWork from '../../../images/icon_howitswork.png';
import './style.scss';

export default class Content extends React.PureComponent {
    render() {
        return (<div className="quick-search">
        <div className="header-mock">
        </div>
        <div className="main-content">
          <h1>Quick Search for Your <span className="green">Perfect Product</span> </h1>
          <p>INNOVATE • LIST • CoNNECT</p>
          <div className="search-container">
            <div className="select-category-container category"> 
              <select name="category"className="category roleway-medium">
                <option defaultValue="" hidden>Category</option>
              </select>
            </div>
            <input type="text" className="input-text roleway-medium"/>
            <input type="button" className="button" value="START SEARCHING"/>
          </div>
        </div>
        <div className="bottom-content">
          <Link to="/" className="button-link"> 
            <img src={AllCategories} alt=""/>
            <span  className="link green">All categories</span>
          </Link>
          <Link to="/"  className="button-link"> 
            <img src={HowItsWork} className="how-its-work" alt="" />
            <span className="link green">How it works?</span>
          </Link>
        </div>
        </div>);
    }
}