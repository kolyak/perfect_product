import React from 'react';
import Slider from "react-slick";
import Customer from '../../../components/Customer';
import John from '../../../images/face_middle.png';
import Adam from '../../../images/face_left.png';
import Oleh from '../../../images/face_right.png';

import Hubstaff from '../../../images/product1.png';
import Jira from '../../../images/product2.png';
import Slack from '../../../images/product3.png';
import TraveChat from '../../../images/product4.png';
import Network from '../../../images/product5.png';
import TimeCamp from '../../../images/product6.png';
import './style.scss';

export default class Content extends React.Component<any, any> {   
  constructor(props: any){
    super(props);
    this.state = {
      productSources: [
        [Hubstaff, "Hubstaff Directory"],
        [Jira, "Jira 2017"], 
        [Slack, "Slack Pro"],
        [TraveChat, "TraveChat"],
        [Network, "Network"],
        [TimeCamp, "TimeCamp"]
      ],
    };
  }
  render() {
    var settings = {
      centerMode: true,
      centerPadding: '25%',
      slidesToShow: 1,
      responsive: [
        {
          breakpoint: 1500,
          settings: {
            centerMode: true,
            centerPadding: '16%',
            slidesToShow: 1
          }
        },
        {
          breakpoint: 1020,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1
          }
        }
      ]
    }
      return (<div className="testimonials">
        <h2>Testimonials</h2>
        <p>What our customers told about us</p>
        <div className="slider-container"> 
          <Slider {...settings}>
            <div> 
              <Customer name="John Kamman" source={John} text="Some text1" position="Wholesum Founder"/>
            </div>
            <div> 
              <Customer name="Name" source={Adam} text="Some text2" position="Position"/>
            </div>
            <div> 
              <Customer name="Name" source={Oleh} text="Some text3" position="Position"/>
            </div>
          </Slider>
        </div>
      </div>);
  }
}
