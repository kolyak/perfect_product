import { Link }  from 'react-router-dom';
import React from 'react';
import Product from '../../../components/Product';
import Hubstaff from '../../../images/product1.png';
import Jira from '../../../images/product2.png';
import Slack from '../../../images/product3.png';
import TraveChat from '../../../images/product4.png';
import Network from '../../../images/product5.png';
import TimeCamp from '../../../images/product6.png';
import './style.scss';

interface ProductState  {
  src: string;
  name: string;
}
interface ProductSourceState {
  products: ProductState[];
}
export default class Content extends React.Component<any, ProductSourceState> {   
  constructor(props: any){
    super(props);
    this.state  = {products: [
        {src: Hubstaff, name: "Hubstaff Directory"},
        {src: Jira, name: "Jira 2017"}, 
        {src: Slack, name: "Slack Pro"},
        {src: TraveChat, name: "TraveChat"},
        {src: Network, name: "Network"},
        {src: TimeCamp, name: "TimeCamp"}
      ]};
  }
  render() {
      return (<div className="most-popular-products">
        <h2>Most Popular Products</h2>
        <p>This is the list of products that are most interested in users</p>
        <div className="products-container"> 
          {this.state.products.map((item: any, index: number) => (
            <Product  key={index} name={item.name} source={item.src}/>
          ))}
        </div>
        
        <Link to="/">All products</Link>
      </div>);
  }
}
