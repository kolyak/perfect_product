import { Link }  from 'react-router-dom';
import React from 'react';
import FooterLogo from '../../../images/footer.png';
import './style.scss';


export default class Content extends React.PureComponent {   

  render() {
      return (<div className="footer">
        <div className="top-content">
          <div className="img-container">
            <img src={FooterLogo} alt="Logo"/>
          </div>
          <div className="links-container"> 
            <Link to="/">ABOUT US</Link>
            <Link to="/">TERMS OF SERVICE</Link>
            <Link to="/">PRIVACY POLICY </Link>
            <Link to="/">CONTACT US</Link>
          </div>
        </div>
        <div className="bottom-content">
          <p>&copy; 2018 Find Parrot</p>
        </div>
      </div>);
  }
}
