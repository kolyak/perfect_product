import React from 'react';
import './style.scss';
import QuickSearch from './QuickSearch';
import Header from '../../components/Header';
import Testimonials from './Testimonials';
import Footer from './Footer';
import MostPopularProducts from './MostPopularProducts';

export default class Content extends React.PureComponent {
    render() {
        return (<div className="App">
          <Header/>
          <QuickSearch/>
          <MostPopularProducts/>
          <Testimonials/>
          <Footer/>
        </div>);
    }
}