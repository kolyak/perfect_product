import React from 'react';
import Content from './content';
import Header from '../../components/Header';
import api from '../../services/api';
import { toast } from 'react-toastify';

export interface UserProfile {
  id: number;
  name: string;
  city: {
    id: number;
    name: string;
  }
  email: string;
  nameValid: boolean;
}

export default class Profile extends React.Component<any, UserProfile> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: "",
      nameValid: false,
      city: {
        id: 0,
        name: ""
    }} as UserProfile;
  }

  updateUser = async (event: Event) => {
    event.preventDefault();
    if(!this.state.nameValid) {
      toast.error("Fill fields correctly!");
      return;
    }
    try {
      let responce = await api.patch("/api/users",
       {user: {
         name: this.state.name
       }});
      this.setState(responce.data);
      toast.success("Profile updated!");
    } catch (error) {
      this.props.history.push("/log-in");
      toast.error(""+error);
    }
  }

  cityChanged = (event: any) => {
    this.setState({ city: {
      id: event.target.value}} as UserProfile);  
  }

  nameChanged = (event: any) => {
    this.setState({
      name: event.target.value,
      nameValid: (event.target.value.length >= 3 && event.target.value.match(/^[a-zA-Z]+$/))
    });
  }

  render() {
    return <div>  
      <Header/>
      <Content {...this.state} updateUser={this.updateUser} cityChanged={this.cityChanged} nameChanged={this.nameChanged}/>
      </div>
  }
}