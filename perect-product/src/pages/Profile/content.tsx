import React from 'react';
import './style.scss';
import { UserProfile } from './index';

interface PropsFunc{
  nameChanged: (e: any) => void;
  cityChanged: any;
  updateUser: any;
  nameValid: boolean;
}
interface Props extends PropsFunc, UserProfile {}

export default class Content extends React.Component< Props, any> {
  
  render() {
    return (<div className="new-product-form">
      <form onSubmit={this.props.updateUser}>
        <h2>Update Profile</h2>
        <label htmlFor="upd_name">Name:</label>
        {(!this.props.nameValid) ?
          <span style={{color: 'red'}}>Email is uncorrect</span> : null
          }
        <input type="text" id="upd_name" value={this.props.name} onChange={this.props.nameChanged}/>
        <label htmlFor="upd_city_id">City id:</label>
        <input type="text" id="upd_city_id" value={this.props.city.id} onChange={this.props.cityChanged}/>
        <label htmlFor="upd_city_name">City name:</label>
        <input type="text" id="upd_city_name" value={this.props.city.name} readOnly/>
        <label htmlFor="upd_city_name" >Email:</label>
        <input type="text" id="upd_city_name" value={this.props.email} readOnly/>
        <input type="submit" className="submit-btn" value="Update"/>
      </form>
    </div>);
  }
}