import React from 'react';
import Content from './content';
import api from '../../services/api';
import Header from '../../components/Header';
import { toast } from 'react-toastify';

interface SignState {
  formType: string;
  email: string;
  password: string;
  confirmPassword: string;
  passwordValid: boolean;
  emailValid: boolean;
  formValid: boolean;
}

export interface User {
  user? : {
    email: string;
    password: string;
  },
  auth? : {
    email: string;
    password: string;
  }
}

export default class Sign extends React.Component<any, SignState> {
  constructor(props: any) {
    super(props);
    let formType = (props.location.pathname === "/log-in") ? "Log In":"Sing Up";
    this.state = {
      formType: formType,
      email: "",
      password: "",
      confirmPassword: "",
      passwordValid: true,
      emailValid: true,
      formValid: false
    }
  }  
  
  componentDidUpdate(prevProps: any, prevState: SignState) {
    if(prevProps.location.pathname !== this.props.location.pathname)
    {
      this.setState({
        formType: ( this.props.location.pathname === "/log-in") ? "Log In":"Sing Up",
      });
    }
    if(prevState.email !== this.state.email || prevState.password !== this.state.password ) {
      this.isFormValid();
    }
  }
  handleSubmit = async (event: Event) => {
    event.preventDefault();
    this.isFormValid();
    if(!this.state.formValid) {
      toast.error("Fill fields correctly!");
      return;
    }
    let path = "";
    var data: any;
    var email: string;
    if (this.props.location.pathname === "/log-in") {
      path = "api/user_token";
      data = this.logIn();
      email = data.auth.email;
    } else {
      if(this.state.confirmPassword !== this.state.password)
      {
        toast.error("Passwords do not match!");
        return;
      }
      path = "api/sign_up";
      data = this.signUp();
      email = data.user.email;
    }
    
    try {
    let responce = await api.post(path, data);
    api.save(email, responce.data.jwt);
    this.props.history.push("/");
    toast.success("Welcome "+email+"!");
    } catch (error) {
      toast.error(""+error);
      return;
    }
  }

  logIn(): User {
    return {auth: {
      email: this.state.email,
      password: this.state.password
      }
    }
  }

  signUp(): User {
    return {user: {
      email: this.state.email,
      password: this.state.password,
      }
    }
  }

  handleUserInput = (event: any) => {
    switch (event.currentTarget.name) {
      case "email":
        this.setState({
          email: event.target.value,
          emailValid: (!!event.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i))
        });
        break;
      case "password":
        this.setState({
          password: event.target.value,
          passwordValid: event.target.value.length >= 6
        })
        break;
        case "confirm-password":
        this.setState({confirmPassword: event.target.value})
        break;
      default:
        break;
    }
  }

  isFormValid() {
    this.setState({formValid: (this.state.emailValid && this.state.passwordValid) });
    let v = this.state.formValid;
    if(this.state.email.length < 1 || this.state.password.length < 1 ) {
      this.setState({formValid: false});
    }
  }
  render() {
    return <div>
      <Header/>
      <Content 
        handleSubmit={this.handleSubmit} 
        handleUserInput={this.handleUserInput}
        formType={this.state.formType}
        emailValid={this.state.emailValid}
        passwordValid={this.state.passwordValid}
        />
      </div>
  }
}