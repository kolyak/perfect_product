import React from 'react';
import './style.scss';

interface SignProps {
  handleSubmit: any;
  handleUserInput: any;
  formType: string;
  emailValid: boolean;
  passwordValid: boolean;
}

export default class Content extends React.Component<SignProps> {
    render() {
      return (<div className="sign-form">
        <form onSubmit={this.props.handleSubmit}>
          <label htmlFor="email">Email:</label>
          {(!this.props.emailValid) ?
          <span style={{color: 'red'}}>Email is uncorrect</span> : null
          }
          <input type="email" className="email" name="email" id="email" onSubmit={this.props.handleUserInput} onChange={this.props.handleUserInput}/>
          <label htmlFor="password">Password:</label>
          {(!this.props.passwordValid) ?
          <span style={{color: 'red'}}>Password is uncorrect</span> : null
          }
          <input type="password" className="password" name="password" id="password" onSubmit={this.props.handleUserInput} onChange={this.props.handleUserInput}/>
          {(this.props.formType === "Sing Up") ? 
            <label htmlFor="password">Confirm Password: </label>
          : null
          }
          {(this.props.formType === "Sing Up") ? 
            <input type="password" className="password" name="confirm-password" id="password" onChange={this.props.handleUserInput}/>
          : null
          }

          <input type="submit" className="submit-btn" value={this.props.formType}/>
        </form>
      </div>);
    }
}