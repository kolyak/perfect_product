import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import React from 'react';
import Landing from '../pages/Landing';
import NotFound from '../pages/NotFound';
import Sign from '../pages/Sign';
import NewProduct from '../pages/NewProduct';
import Profile from '../pages/Profile';
import 'react-toastify/dist/ReactToastify.css';

export default class Router extends React.PureComponent{
    render(){
       return (
       <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/log-in" component={Sign} />
                <Route exact path="/sign-up" component={Sign} />
                <Route exact path="/new-product" component={NewProduct} />
                <Route exact path="/profile" component={Profile} />
                <Route path="/404" component={NotFound} />
                <Redirect to="/404" />
            </Switch>
        </BrowserRouter>);
    }
}
