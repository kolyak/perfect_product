export namespace ValidationRules {
  function allowedExtensions(value: any, fileTypes: any): any;
  function isEmail(value: any): any;
  function isEmpty(value: any): any;
  function isFile(value: any): any;
  function isFloat(value: any): any;
  function isNumber(value: any): any;
  function isPositive(value: any): any;
  function isString(value: any): any;
  function matchRegexp(value: any, regexp: any): any;
  function maxFileSize(value: any, max: any): any;
  function maxFloat(value: any, max: any): any;
  function maxNumber(value: any, max: any): any;
  function maxStringLength(value: any, length: any): any;
  function minFloat(value: any, min: any): any;
  function minNumber(value: any, min: any): any;
  function minStringLength(value: any, length: any): any;
  function required(value: any): any;
  function trim(value: any): any;
}
export class ValidatorComponent {
  static getDerivedStateFromProps(nextProps: any, prevState: any): any;
  constructor(...args: any[]);
  componentDidMount(): void;
  componentDidUpdate(prevProps: any, prevState: any): void;
  componentWillMount(): void;
  componentWillReceiveProps(nextProps: any): void;
  componentWillUnmount(): void;
  forceUpdate(callback: any): void;
  setState(partialState: any, callback: any): void;
  shouldComponentUpdate(nextProps: any, nextState: any): any;
}
export namespace ValidatorComponent {
  namespace contextTypes {
    function form(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace form {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
  }
  namespace defaultProps {
    const errorMessages: string;
    function validatorListener(): void;
    const validators: any[];
  }
  namespace propTypes {
    function errorMessages(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace errorMessages {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function validatorListener(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace validatorListener {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function validators(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace validators {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function value(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace value {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function withRequiredValidator(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace withRequiredValidator {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
  }
}
export class ValidatorForm {
  static addValidationRule(name: any, callback: any): void;
  static getValidator(validator: any, value: any, includeRequired: any): any;
  constructor(...args: any[]);
  forceUpdate(callback: any): void;
  render(): any;
  setState(partialState: any, callback: any): void;
}
export namespace ValidatorForm {
  namespace childContextTypes {
    function form(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace form {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
  }
  namespace defaultProps {
    const debounceTime: number;
    function onError(): void;
  }
  namespace propTypes {
    function children(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace children {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function debounceTime(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace debounceTime {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function instantValidate(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace instantValidate {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function onError(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    namespace onError {
      function isRequired(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
    }
    function onSubmit(p0: any, p1: any, p2: any, p3: any, p4: any, p5: any): any;
  }
}
